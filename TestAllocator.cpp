// -----------------
// TestAllocator.cpp
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator
#include <string>    // string

#include <iostream>

#include "gtest/gtest.h"

#include "Allocator.hpp"

struct A {
    friend bool operator == (const A&, const A&) {
        return true;}

    static std::string s;

    A  ()         {s.push_back('A');}
    A  (int)      {s.push_back('B');}
    A  (const A&) {s.push_back('C');}
    ~A ()         {s.push_back('D');}};

std::string A::s;

template class my_allocator<A, 1000>;

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<A>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    A::s.clear();

    allocator_type   x;
    const size_type  s = 2;
    const value_type v = 0;
    const pointer    b = x.allocate(s);
    ASSERT_EQ(A::s, "B");

    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}
            ASSERT_EQ(A::s, "BCC");}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);
        ASSERT_EQ(A::s, "BCCDD");}}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<A, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    A::s.clear();

    allocator_type   x;
    const size_type  s = 2;
    const value_type v = 0;
    const pointer    b = x.allocate(s);
    ASSERT_EQ(A::s, "B");

    if (b != nullptr) {
        std::cout << "hi" << std::endl;
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}
            ASSERT_EQ(A::s, "BCC");}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);
        ASSERT_EQ(A::s, "BCCDD");}}

/*                                                               // uncomment
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<A, 1000>
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 0);}                                         // fix test

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<A, 1000>
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 0);}                                         // fix test
*/
