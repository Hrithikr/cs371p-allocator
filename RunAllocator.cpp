// ----------------
// RunAllocator.cpp
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */
    cout << "-40 944"     << endl;
    cout << "-40 -24 912" << endl;
    cout << "40 -24 912"  << endl;
    cout << "72 -24 880"  << endl;
    return 0;}
